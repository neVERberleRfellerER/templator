<%@ def.overlayTest:
Zkouška překrytí - pokud je toto vidět, je to rozbitý.
%>

<html>
  <head>
    <meta charset="utf-8">
    <title><%= ctx.title %></title>
  </head>
  <body>
    Nejaka (asynchronni: <%= yield ctx.asyncVal2 %>) sracka na ukazku
    <%@ def.body2 || '' %>
  </body>
</html>
