<%# layoutfront %>

<%@ def.title:
Název stránky
%>

<%@ def.overlayTest:
Zkouška překrytí - pokud je toto vidět, funguje to správně!
%>

<%@ def.body:
<div>
  <div><%= ctx.name %></div>
  <%- for (let i of [1,2,3,4]) { %>
    <span><%= i %></span>
  <%- } %>
  <p>Něco asynchroniho pro Vás: <%= yield ctx.asyncVal %>!
  Zkouška escapování: `! \` \`` `;;+</p>
</div>
%>
