"use strict";

const fs = require('fs');
const bluebird = require('bluebird');

const readFileAsync = bluebird.promisify(fs.readFile, fs);

function pdelay(msec) {
  return new Promise(function (resolve) {
    setTimeout(resolve, msec);
  });
}

function measurement() {
  return function* (next) {
    const start = process.hrtime();
    yield* next;
    const diff = process.hrtime(start);

    const msecs = diff[0] * 1e3 + diff[1] / 1e6;
    this.set('X-Response-Time', `${msecs}`);
  };
}

(bluebird.coroutine(function* () {
  const Templator = require('./templator').Templator;
  const templator = new Templator();
  let tfn = templator.compile(yield readFileAsync('tpl2.tpl', 'utf-8'), {
    layoutfront: yield readFileAsync('tpl1.tpl', 'utf-8'),
    layoutfront2: yield readFileAsync('tpl3.tpl', 'utf-8')
  });

  tfn = bluebird.coroutine(tfn);

  // koa sample
  let app = require('koa')();
  app.use(function* (next) {
    if (this.request.url !== '/') return;
    yield* next;
  });
  app.use(measurement());
  app.use(function* () {
    this.type = 'text/html';
    this.body = yield tfn({
      title: 'Muj nazev stranky',
      name: 'jmeno',
      asyncVal: pdelay(30).then(function () { return 5; }),
      asyncVal2: pdelay(50).then(function () { return 10; })
    });
  });
  app.listen(8000);
})());
