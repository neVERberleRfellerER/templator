"use strict";

const bluebird = require('bluebird');
const objectAssign = require('object-assign');

function T() {
};

T.prototype._compileTimePreprocess = function (src, defs) {
  let res = src;

  // partials
  defs = objectAssign({}, defs);
  let partialRe = /\s*^<%@ (.*?):\n([\s\S]*?)\n%>$\s*/gm;
  res = res.replace(partialRe, function (m, name, body) {
    if (typeof defs[name] === 'undefined')
      defs[name] = body;
    return '';
  });

  // layouting
  let match = res.match(/<%# (.*?) %>/);
  if (match !== null) {
    let layout = defs[match[1]];
    let tmp = this._compileTimePreprocess(layout, defs);
    res = tmp[0];
    defs = tmp[1];
  }

  // partial replacement
  let partialReplacementRe = /<%@( .*? )%>/g;
  res = res.replace(partialReplacementRe, function (m, expr) {
    let replacement = new Function('defs', `
      return ${expr.replace(/\s(def\..*?)\s/, 'defs["$1"]')};
    `)(defs);
    if (typeof replacement !== 'undefined')
      return replacement;
    return '!!!UNDEFINED PARTIAL: ' + expr + ' !!!';
  });

  return [res, defs];
};

T.prototype.compile = function (src, defs) {
  let body = src;

  // compile-time stuff
  body = this._compileTimePreprocess(body, defs)[0];

  // runtime stuff
  body = body
    .replace(/`|\\/g, "\\$&")
    .replace(/<%\- ([\s\S]+?((%>)?)+) %>/g, '`;$1 out+=`')
    .replace(/<%= ([\s\S]+?((%>)?)+) %>/g, '`;out+=($1);out+=`');

  let fun = `
    return function* (ctx) {
      "use strict";
      let out = '';
      out += \`${body}\`;
      return out;
    };
  `;

  try {
    return new Function(fun)();
  } catch (err) {
    throw new Error('Could not create template function: ' + fun);
  }
};

exports.Templator = T;
